# Project 2

Eric added a line here. 
This repository has some template data files and starter code for our Spring 2017 project #2.

The current example code can be invoked with:

$ python plot.py spectra/Sp15_245L_sect-001_group-2-4_spectrum-H2

and it currently will read in the spectrum data file into a numpy array, and verify it has done so as expected by printing it out.
